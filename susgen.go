// Copyright (2016) Alok Parlikar.  All rights reserved.
// Use of this code is governed by the MIT License that
// can be found in the LICENSE file.

// Package susgen implements a language generator for semantically unpredictable
// sentences.  It uses the MOBY part of speech database to generate sentences
// based on fixed patterns.
package susgen

import (
	"bufio"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strings"
)

// posmap stores a list of words for each part of speech as its key
type posmap map[string][]string

// generate takes a posmap data structure and returns a randomly generated
// sentence as a string
func Generate(p posmap) (string, error) {
	if p == nil {
		return "", errors.New("Unable to generate sentence. MOBY data not loaded.")
	}

	adjlist, ok := p["A"]
	if !ok {
		return "", errors.New("Unable to generate sentence. Adjectives not found in MOBY list.")
	}

	nounlist, ok := p["N"]
	if !ok {
		return "", errors.New("Unable to generate sentence. Nouns not found in MOBY list.")
	}

	verblist, ok := p["t"]
	if !ok {
		return "", errors.New("Unable to generate sentence. Verbs not found in MOBY list.")
	}

	adj1 := adjlist[rand.Intn(len(adjlist))]
	adj2 := adjlist[rand.Intn(len(adjlist))]

	noun1 := nounlist[rand.Intn(len(nounlist))]
	noun2 := nounlist[rand.Intn(len(nounlist))]

	verb := verblist[rand.Intn(len(verblist))]

	result := fmt.Sprintf("The %s %s %s the %s %s.", adj1, noun1, verb, adj2, noun2)

	return result, nil
}

// load MOBY data from a file and return a posmap
func LoadMoby(filename string) (posmap, error) {
	p := make(posmap)

	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		toks := strings.Split(scanner.Text(), "\t")
		if len(toks) != 2 {
			continue
		}
		t := toks[1]
		w := toks[0]

		// If a word is a verb, only choose those ending in 'ed'
		// This makes subject-verb agreement easier during sentence generation.
		if t == "t" {
			if !strings.HasSuffix(w, "ed") {
				continue
			}
		}

		// If a word is a noun or adjective, only choose those starting with small alphabet
		if t == "N" || t == "A" {
			if strings.ToLower(w) != w {
				continue
			}
		}

		p[t] = append(p[t], w)
	}
	return p, nil
}
