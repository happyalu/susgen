// Copyright (2016) Alok Parlikar.  All rights reserved.
// Use of this code is governed by the MIT License that
// can be found in the LICENSE file.

// Package susgen implements a language generator for semantically unpredictable
// sentences.  It uses the MOBY part of speech database to generate sentences
// based on fixed patterns.

package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"

	"bitbucket.org/happyalu/susgen"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	filenamePtr := flag.String("moby", "mobyposi.i", "Path to mobyposi.i file")
	numSentPtr := flag.Int("num", 1, "How many sentences to generate")

	flag.Parse()

	p, err := susgen.LoadMoby(*filenamePtr)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	for i := 0; i < *numSentPtr; i++ {
		s, err := susgen.Generate(p)
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println(s)
	}
}
